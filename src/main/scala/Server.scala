import java.net.InetSocketAddress
import java.time.LocalDateTime

import akka.actor.{ActorSystem, Props}
import io.stock.Protocol._
import io.stock._

/**
  * Main-class приложения
  * запуск как адрес_источника_данных:порт адрес_ожидания_клиентов:порт
  * sbt "run localhost:5000 localhost:3000"
  */
object Server {
  def main(args: Array[String]): Unit = {
    if (args.length != 2) {
      println("Start as 'sbt \"run localhost:5555 localhost:8080\"'")
      System.exit(1)
    }

    def parseHost(line: String) = {
      val arr = line.split(":")
      (arr(0), arr(1).toInt)
    }

    val (dataHost, dataPort) = parseHost(args.head)
    val (listenHost, listenPort) = parseHost(args.last)

    startServer(dataHost, dataPort, listenHost, listenPort)
  }

  def startServer(dataHost: String, dataPort: Int, listenHost: String, listenPort: Int) = {
    implicit val system = ActorSystem("Sales")
    implicit val dispatcher = system.dispatcher

    val aggregator = system.actorOf(Props(new AggregatorActor()), "aggregator")
    val client = system.actorOf(Props(new ClientsListener(aggregator, listenHost, listenPort)), "clients")
    val upstream = system.actorOf(Props(new UpstreamActor(aggregator, dataHost, dataPort)), "upstream")
    aggregator ! Protocol.BroadcastSender(client)

    system.actorOf(Props(classOf[akka.Main.Terminator], aggregator), "app-terminator")
  }
}
