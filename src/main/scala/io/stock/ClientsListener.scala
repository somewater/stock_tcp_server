package io.stock

import java.net.InetSocketAddress
import java.time.{LocalDateTime, ZoneOffset}

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.io.Tcp._
import akka.io.{IO, Tcp}
import akka.util.ByteString
import io.stock.Protocol.Candles

/**
  * Актор взаимодействия с подключившимися клиентами
  */
class ClientsListener(aggregator: ActorRef, host: String, port: Int) extends Actor with ActorLogging {

  import context.system

  override def preStart(): Unit = {
    IO(Tcp) ! Bind(self, new InetSocketAddress(host, port))
  }

  def receive = {
    case Bound(_) =>
      log.info(s"Listen client connections on ${host}:${port}")

    case CommandFailed(_: Bind) =>
      aggregator ! Protocol.ConnectionRefused

    case Connected(_, _) =>
      val connection = sender()
      val handler = context.actorOf(Props(new ClientHandler(connection, aggregator)))
      connection ! Register(handler)

    case candles: Candles =>
      context.children.foreach(_ ! candles)
  }
}

sealed class ClientHandler(connection: ActorRef, aggregator: ActorRef) extends Actor {

  val sb = StringBuilder.newBuilder

  override def preStart(): Unit = {
    aggregator ! Protocol.ClientConnected(LocalDateTime.now(ZoneOffset.UTC))
  }

  def receive = {
    case Protocol.Candles(candles) =>
      candles.foreach {
        c =>
          sb.append(c.toJson())
          sb.append('\n')
      }
      connection ! Write(ByteString(sb.result()))
      sb.clear()

    case PeerClosed =>
      aggregator ! Protocol.ClientDisconnected
      context stop self
  }
}


