package io.stock

import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.time.{Instant, LocalDateTime, ZoneOffset}
import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorLogging, ActorRef}
import akka.io.{IO, Tcp}
import akka.io.Tcp._

import scala.concurrent.duration.FiniteDuration

/**
  * Актор для получения и разбора данных из источника
  */
class UpstreamActor(consumer: ActorRef, host: String, port: Int) extends Actor with ActorLogging {

  import context.system
  import context.dispatcher

  val buffer = ByteBuffer.allocate(1024)
  val sb = StringBuilder.newBuilder

  def connect() = {
    IO(Tcp) ! Connect(new InetSocketAddress(host, port))
  }

  override def preStart(): Unit = {
    connect()
  }

  def receive = waitConnection()

  def waitConnection(): Receive = {
    case Reconnect =>
      connect()

    case CommandFailed(_: Connect) =>
      consumer ! Protocol.ConnectionRefused
      context stop self

    case Connected(_, _) =>
      log.info(s"Connected to upstream on ${host}:${port}")
      sender() ! Register(self)
      context.become(listenUpstream(sender()))
  }

  def listenUpstream(connection: ActorRef):  Receive = {
    case Received(data) =>
      data.copyToBuffer(buffer)
      tryToSendTrades()
    case _: ConnectionClosed =>
      log.warning("Upstream connection closed, reconnect")
      context.become(waitConnection())
      context.system.scheduler.scheduleOnce(FiniteDuration(1, TimeUnit.SECONDS), self, Reconnect)
    case CommandFailed(cmd) =>
      log.warning(s"Upstream command failed: ${cmd}")
      // TODO: resend data?
  }

  def tryToSendTrades(): Unit = {
    buffer.flip()
    buffer.mark()
    // минимальная длина корректной записи о сделке - не менее 24 байт (допуская пустое название для тикера)
    while (buffer.remaining() >= 24) {
      val len = buffer.getChar().toShort
      if (buffer.remaining() + 2 >= len) {
        val timestamp = buffer.getLong()
        val tickerLen = buffer.getChar.toShort
        for (i <- 1 to tickerLen ) {
          sb.append(buffer.get().toChar)
        }
        val price = buffer.getDouble()
        val size = buffer.getInt()

        val t = createTrade(timestamp, sb.result(), price, size)
        consumer ! t
        sb.clear()
        buffer.mark()
      } else {
        buffer.reset()
        buffer.compact()
        return
      }
    }

    if (buffer.remaining() > 0) {
      buffer.reset()
      buffer.compact()
    } else {
      buffer.clear()
    }
  }

  def createTrade(timestamp: Long, ticker: String, price: Double, size: Int) =
    Protocol.Trade(ticker, LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), ZoneOffset.UTC), price, size)
}

object Reconnect
