package io.stock

import java.time.{LocalDateTime, ZoneOffset}
import java.time.temporal.ChronoUnit

import akka.actor.{Actor, ActorLogging, ActorRef, Cancellable}
import Protocol._

import concurrent.duration._
import scala.concurrent.duration.FiniteDuration
import collection.mutable
import scala.collection.immutable.TreeMap

/**
  * Актор, который занимается собственно логикой: обработкой и пересылкой поступающих данных, их накоплением
  * для отсылки 10-ти минутной истории новым клиентам и актуального состояния ежеминутно.
  */
class AggregatorActor extends Actor with ActorLogging {

  import context.dispatcher

  implicit val dateTimeOrdering = new Ordering[LocalDateTime] {
    override def compare(x: LocalDateTime, y: LocalDateTime): Int = x.compareTo(y)
  }

  var scheduling: Option[Cancellable] = None

  // history(<time>)(<ticker name>) == CandleBuilder
  var history = new TreeMap[LocalDateTime, mutable.HashMap[String, CandleBuilder]]

  // Актор, которому посылаются широковещательные запросы для отсылки всем клиентам
  var broadcastClients: Option[ActorRef] = None

  override def preStart(): Unit = {
    startScheduling()
  }

  def startScheduling() = {
    val secondsNow = now().getSecond
    val c = context.system.scheduler.schedule((60 - secondsNow) second, 1 minute, self, SendCandles)
    scheduling = Some(c)
  }

  override def postStop(): Unit = {
    scheduling.foreach(_.cancel())
    scheduling = None
  }

  def receive = {
    case BroadcastSender(broadcast) =>
      broadcastClients = Some(broadcast)
    case t: Trade =>
      addTrade(t)
    case SendCandles =>
      sendCandles()
      clearOutdatedCandles()
    case ConnectionRefused =>
      log.error("Connection refused")
      context.stop(self)

    case ClientConnected(startTime) =>
      val now = truncate(startTime)
      val fromTime = now.minusMinutes(10)
      val buf = collection.mutable.ListBuffer.empty[Candle]
      history.foreach {
        case (time, hm) =>
          if (fromTime.compareTo(time) <= 0 && now.compareTo(time) > 0)
            buf ++= hm.values.map(_.result())
      }
      if (buf.nonEmpty) {
        log.info(s"Client connected, send ${buf.size} candles")
        sender() ! Candles(buf.result())
      } else {
        log.info("Client connected, no data for send")
      }

    case ClientDisconnected =>
      log.info("Client disconnected")

  }

  def addTrade(trade: Trade) = {
    val time = truncate(trade.timestamp)
    val byTicker = history.get(time) match {
      case Some(hm) => hm
      case None =>
        val hm = new mutable.HashMap[String, CandleBuilder]
        val kv = (time, hm)
        history = history + kv
        hm
    }
    val cb = byTicker.getOrElseUpdate(trade.ticker,  new CandleBuilder(trade.ticker, time))
    cb.addTrade(trade)
  }

  def sendCandles() = {
    broadcastClients match {
      case Some(broadcast) =>
        val time = truncate(now()).minusMinutes(1)
        history.get(time) match {
          case Some(hm) =>
            broadcast ! Candles(hm.values.map(_.result()).toList)
          case None =>
          // сбор данных только начался, данных за прошедшую минуту еще нет
        }
      case None =>
    }
  }

  def clearOutdatedCandles() = {
    val oldest = truncate(now()).minusMinutes(10)
    while(history.nonEmpty && history.firstKey.compareTo(oldest) < 0) {
      history = history.tail
    }
  }

  private def truncate(l: LocalDateTime) = l.truncatedTo(ChronoUnit.MINUTES)

  protected def now() = LocalDateTime.now(ZoneOffset.UTC)
}

object SendCandles

class CandleBuilder(ticker: String, timestamp: LocalDateTime) {
  private var open = -1D
  private   var close = -1D
  private var high = -1D
  private var low = -1D
  private var volume = 0

  def addTrade(t: Trade) = {
    if (open < 0)
      open = t.price
    if (high < t.price)
      high = t.price
    if (low < 0 || low > t.price)
      low = t.price
    close = t.price
    volume += t.size
  }

  def result() = Candle(ticker, timestamp, open, high, low, close, volume)
}
