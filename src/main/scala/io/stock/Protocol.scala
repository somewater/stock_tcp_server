package io.stock

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import akka.actor.ActorRef
import org.json4s._
import org.json4s.native.Serialization
import org.json4s.native.JsonMethods._
import org.json4s.JsonDSL._

/**
  * Структуры, которыми обмениваются акторы:
  * [UpstreamActor] -(Trade)->
  *   |                         [               ]  --- (Candles 1 min)  --> [ClientsListener]-----
  *   |                         [               ]                                /              /
  *   |                         [               ]                               v              v
  *   |                         [               ]  <-(ClientConnected)---- [             ]   [ClientHandler] ...
  *   |                         [AggregatorActor]  --- (Candles 10 min)--> [ClientHandler]
  *   |                         [               ]  <-(ClientDisconnected)- [             ]
  *   |                         [               ]
  *   |---(ConnectionRefused)-->[               ] -> <emergency stop>
  */
object Protocol {
  case object ConnectionRefused // если не удалось соединиться с updtream или начать слушать порт для клиентов -
                                // в обоих случаях сервер не может функционировать и останавливается

  case class ClientConnected(startTime: LocalDateTime)
  case class ClientDisconnected()
  case class BroadcastSender(actor: ActorRef)

  case class Trade(ticker: String, timestamp: LocalDateTime, price: Double, size: Int)
  case class Candle(ticker: String,
                    timestamp: LocalDateTime,
                    open: Double,
                    high: Double,
                    low: Double,
                    close: Double,
                    volume: Int) {
    def toJson() = Serialization.write(this)
  }
  case class Candles(candles: List[Candle])

  implicit val formats = Serialization.formats(NoTypeHints) + LocalDateTimeSerializer

  object LocalDateTimeSerializer extends Serializer[LocalDateTime] {
    private val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")

    override def serialize(implicit format: Formats): PartialFunction[Any, JValue] = {
      case dateTime: LocalDateTime =>
        JString(formatter.format(dateTime))
    }

    override def deserialize(implicit format: Formats): PartialFunction[(TypeInfo, JValue), LocalDateTime] = ???
  }
}
