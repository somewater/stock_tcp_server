package io.stock

import java.nio.ByteBuffer
import java.time.ZoneOffset

import akka.actor.{ActorSystem, Props}
import akka.io.Tcp
import akka.io.Tcp.{Received, Register}
import akka.testkit.{TestKitBase, TestProbe}
import akka.util.ByteString
import io.stock.Protocol.Trade
import org.scalamock.scalatest.MockFactory
import org.scalatest._
import org.scalatest.words.ShouldVerb

class UpstreamActorTest extends  FreeSpec
                            with TestKitBase
                            with MockFactory
                            with Matchers {
  override implicit lazy val system: ActorSystem = ActorSystem("test")

  abstract class Fixture {
    val aggregator = TestProbe()
    val actor = system.actorOf(Props(new UpstreamActor(aggregator.ref, "hello", 1){
      override def connect(): Unit = ()
    }))
    implicit def self = testActor
  }

  implicit class RichBB(bb: ByteBuffer) {
    def toArray(): Array[Byte] = {
      bb.array().take(bb.position())
    }
  }

  "should correctly parse input bytes" - {
    "when trade separated by different TCP packages" in new Fixture {
      actor ! Tcp.Connected(null, null)
      expectMsgClass(classOf[Register])

      val buf = ByteBuffer.allocate(100)
      // 24 - длина данных о биржевой сделке, если ticker имеет нулевую длину
      val timestamp = 1482078627L
      buf.putChar((24 + 3).toChar)
      buf.putLong(timestamp * 1000)
      buf.putChar(3)
      buf.put('R'.toByte)
      buf.put('U'.toByte)
      buf.put('B'.toByte)
      actor ! Received(ByteString(buf.toArray))

      aggregator.expectNoMsg()

      buf.clear()
      buf.putDouble(234.03)
      buf.putInt(200)
      actor ! Received(ByteString(buf.toArray))

      aggregator.expectMsgPF(){
        case Trade("RUB", time, 234.03, 200) =>
          time.atZone(ZoneOffset.UTC).toEpochSecond should equal(timestamp)
      }
    }

    "when TCP package contains multiple trades" in new Fixture {
      actor ! Tcp.Connected(null, null)
      expectMsgClass(classOf[Register])

      val timestamp = 1482078L
      val buf = ByteBuffer.allocate(100)

      buf.putChar((24 + 5).toChar)
      buf.putLong(timestamp * 1000)
      buf.putChar(5)
      buf.put('A'.toByte)
      buf.put('P'.toByte)
      buf.put('P'.toByte)
      buf.put('L'.toByte)
      buf.put('E'.toByte)
      buf.putDouble(100.01)
      buf.putInt(100)

      buf.putChar((24 + 3).toChar)
      buf.putLong((timestamp + 1) * 1000)
      buf.putChar(3)
      buf.put('E'.toByte)
      buf.put('U'.toByte)
      buf.put('R'.toByte)
      buf.putDouble(200.02)
      buf.putInt(200)

      buf.putChar((24 + 3).toChar)
      buf.putLong((timestamp + 2) * 1000)
      buf.putChar(3)
      buf.put('U'.toByte)
      buf.put('S'.toByte)
      buf.put('D'.toByte)
      buf.putDouble(300.03)
      buf.putInt(300)

      actor ! Received(ByteString(buf.toArray))

      aggregator.expectMsgPF(){
        case Trade("APPLE", time, 100.01, 100) =>
          time.atZone(ZoneOffset.UTC).toEpochSecond should equal(timestamp)
      }

      aggregator.expectMsgPF(){
        case Trade("EUR", time, 200.02, 200) =>
          time.atZone(ZoneOffset.UTC).toEpochSecond should equal(timestamp + 1)
      }

      aggregator.expectMsgPF(){
        case Trade("USD", time, 300.03, 300) =>
          time.atZone(ZoneOffset.UTC).toEpochSecond should equal(timestamp + 2)
      }
    }
  }
}
