package io.stock

import java.time.temporal.ChronoUnit
import java.time.{LocalDateTime, ZoneOffset}

import akka.actor.{ActorSystem, Props}
import akka.testkit.{TestKitBase, TestProbe}
import org.scalamock.scalatest.MockFactory
import org.scalatest._
import Protocol._

class AggregatorActorTest extends  FreeSpec
                            with TestKitBase
                            with MockFactory
                            with Matchers {
  override implicit lazy val system: ActorSystem = ActorSystem("test")

  abstract class Fixture {
    implicit def self = testActor
    val now = LocalDateTime.now(ZoneOffset.UTC).truncatedTo(ChronoUnit.MINUTES)

    val clients = TestProbe()
    val actor = system.actorOf(Props(new AggregatorActor(){
      override def startScheduling(): Unit = ()

      override protected def now(): LocalDateTime = Fixture.this.now
    }))
    actor ! BroadcastSender(clients.ref)
  }

  "should send aggregated candles" - {
    "when server started just now" in new Fixture {
      actor ! ClientConnected(now)

      expectNoMsg()
    }

    "when server works for a long time" in new Fixture {
      actor ! Trade("HELLO", now.minusMinutes(11), 10000, 10000)
      actor ! Trade("WORLD", now.minusMinutes(10), 1000, 1000)

      actor ! Trade("FOO", now.minusMinutes(5), 120, 300)

      actor ! Trade("FOO", now.minusSeconds(150), 100, 120)
      actor ! Trade("FOO", now.minusSeconds(140), 300, 130)
      actor ! Trade("FOO", now.minusSeconds(130), 500, 140)

      actor ! Trade("BAR", now.minusSeconds(110), 1000, 10)
      actor ! Trade("BAR", now.minusSeconds(90), 900, 30)

      actor ! Trade("NOW", now, 1, 2)

      actor ! ClientConnected(now.plusSeconds(5))

      expectMsgPF() {
        case Candles(
          Candle("WORLD", t1, 1000, 1000, 1000, 1000, 1000) ::
          Candle("FOO", t2, 120, 120, 120, 120, 300) ::
          Candle("FOO", t3, 100, 500, 100, 500, 390) ::
          Candle("BAR", t4, 1000, 1000, 900, 900, 40)  :: Nil) =>

          t1 should equal(now.minusMinutes(10))
          t2 should equal(now.minusMinutes(5))
          t3 should equal(now.minusMinutes(3))
          t4 should equal(now.minusMinutes(2))
      }
    }
  }

  "should send every-minute candles" - {
    "when data exists" in new Fixture {
      actor ! Trade("TOO_OLD", now.minusMinutes(3), 1000, 1000)

      actor ! Trade("PREW_MIN", now.minusSeconds(30), 30, 4)
      actor ! Trade("PREW_MIN", now.minusSeconds(20), 10, 5)
      actor ! Trade("PREW_MIN", now.minusSeconds(10), 20, 6)

      actor ! Trade("TOO_FRESH", now, 1000, 1000)
      actor ! Trade("TOO_FRESH", now.plusSeconds(10), 2000, 2000)

      actor ! SendCandles

      clients.expectMsgPF() {
        case Candles(List(Candle("PREW_MIN", t1, 30, 30, 10, 20, 15))) =>
          t1 should equal(now.minusMinutes(1))
      }
    }

    "when no new data during minute" in new Fixture {
      actor ! Trade("HELLO", now.minusMinutes(3), 1000, 1000)
      actor ! SendCandles
      clients.expectNoMsg()
    }
  }
}
