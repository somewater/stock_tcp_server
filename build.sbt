name := "stock_tcp_server"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.4.14",
  "org.json4s" % "json4s-native_2.11" % "3.5.0"
)

// for testing
libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.0.0" % "test",
  "org.scalamock" %% "scalamock-scalatest-support" % "3.3.0" % "test",
  "com.typesafe.akka" %% "akka-testkit" % "2.4.14" % "test"
)